package com.pymes.model;

import java.sql.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "asset")
public class Asset {
	
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="id_asset")
	private int idAsset;
	
	@Column(name= "des_asset")
	private String desAsset; 
	
	@Column(name= "años_vida")
	private int añosVida;
	
	@Column(name="id_proveedor")
	private int idProveedor;
	
	@Column(name="marca")
	private String marca;
	
	@Column(name="serie")
	private String serie;
	
	@Column(name="fecha_Compra")
	private Date fechaCompra;
	
	@Column(name="valor_Compra")
	private double valorCompra; 	
	
	@ManyToMany(targetEntity=Feature.class,
	        cascade={CascadeType.PERSIST, CascadeType.MERGE})
	@JoinTable( name="asset_feature",
	        joinColumns=@JoinColumn(name="id_asset"),
	        inverseJoinColumns=@JoinColumn(name="id_feature")
	)private Set<Feature> listFeautre;
	
	

}
