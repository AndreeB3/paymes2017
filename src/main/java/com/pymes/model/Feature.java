package com.pymes.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "feature")
public class Feature {
	
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="id_feature")
	private int idFeature;
	
	@Column(name = "id_asset")
	private int codAsset;
	
	@Column(name = "name_feature")
	private String nombre; 
	
	@Column(name= "des_feature")
	private String desFeature;
	

}
