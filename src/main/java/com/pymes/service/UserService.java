package com.pymes.service;

import com.pymes.model.User;

public interface UserService {
	
	public User findUserByEmail(String email);
	public void saveUser(User user);
}
